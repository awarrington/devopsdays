require 'spec_helper'

describe 'devopsdays::nginx::ubuntu' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }
      let(:pre_condition) do
        "
        class { 'devopsdays':
          fix_cgi            => false,
          php_extensions     => {},
          fpm_listen_owner => 'foo',
          fpm_listen_group => 'bar',
          fpm_user           => 'foobar',
          fpm_group          => 'baz',
          fpm_skt_dir        => '/foo/bar/baz',
        }
        "
      end

      if os_facts[:os]['name'] == 'Ubuntu'
        it { is_expected.to compile }
      else
        it { is_expected.not_to compile }
      end
    end
  end
end
