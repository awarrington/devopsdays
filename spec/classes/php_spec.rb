require 'spec_helper'

describe 'devopsdays::php' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }
      let(:pre_condition) do
        "
        class { 'devopsdays':
          fix_cgi            => false,
          php_extensions     => {},
          fpm_listen_owner => 'foo',
          fpm_listen_group => 'bar',
          fpm_user           => 'foobar',
          fpm_group          => 'baz',
          fpm_skt_dir        => '/foo/bar/baz',
        }
        "
      end

      it { is_expected.to compile }
      it { is_expected.to contain_class('php') }
      case os_facts[:os]['name']
      when 'Ubuntu'
        packages = if os_facts[:os]['release']['major'] == '18.04'
                     ['php7.2-cli', 'php7.2-common', 'php7.2-fpm']
                   else
                     ['php7.0-cli', 'php7.0-common', 'php7.0-fpm']
                   end
      when %r{^(RedHat|CentOS)$}
        packages = ['php-cli', 'php-common', 'php-cli']
      end

      packages.each do |pkg|
        it { is_expected.to contain_package(pkg).with_ensure('present') }
      end
    end
  end
end
