# Setup IPTables rules to allow HTTP and HTTPS traffic 
#
# @summary Open ports 80 and 443 
#
# @api private
#
# @author David Hollinger <david.hollinger@modulex.com>
#
# @see devopsdays
# @see https://forge.puppet.com/puppetlabs/firewall IPTables module
# @see https://netfilter.org/documentation/ IPTables docs
#
class devopsdays::firewall {
  assert_private()

  class { 'firewall':
    ensure_v6 => 'stopped',
  }

  firewall { '100 allow http traffic':
    ensure   => 'present',
    provider => 'iptables',
    dport    => ['80', '443'],
    proto    => 'tcp',
    action   => 'accept',
  }
}
