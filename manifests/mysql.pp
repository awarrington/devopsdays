# Install and setup MySQL server on Ubuntu and MariaDB on RHEL.
#
# @summary Install and setup MySQL server. 
#
# @api private
#
# @author David Hollinger <david.hollinger@moduletux.com>
#
# @see devopsdays
# @see https://forge.puppet.com/puppetlabs/mysql MySQL Module
# @see https://dev.mysql.com/doc/ MySQL Docs
# @see https://mariadb.com/kb/en/library/documentation/ MariaDB Docs
#
class devopsdays::mysql {
  assert_private()

  contain 'mysql::server'
}
